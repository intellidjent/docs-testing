---
title: Resource Estimation for Local Training
---
To train IE model successfully from local desktop or terminal required to have at least 8GB RAM and 2 CPU. Recommended values are gathered in following table: 

16GB RAM	8GB RAM	16GB RAM	16GB RAM	16GB RAM
4 CPU	4 CPU	8 CPU	8 CPU	8 CPU
* depends on documents volume and amount of selected classes, estimated only for training set size less then 3000 and less than 6 classes. 

For some critical cases with huge volume of data in training set  local training could throw OutOfMemoryException. To avoid that case for all IE models ( Generic or Extended model)  required to refactor ModelTrainingRunner class and train model per field only. Here the code sample bellow:

sdlgknsgknsg
