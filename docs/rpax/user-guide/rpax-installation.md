---
title: RPA Express Installation
---

## Downloading RPA Express

To download RPA Express, follow the instructions below.

1. Open the [WorkFusion website](https://www.workfusion.com/).
2. Click **Get free RPA** in the top-right corner, or select **Products** > **Robotic Process Automation Express – RPA Express**.
3. Click **Download now**.
![](assets/rpax/user-guide/52406313.png)
4. Fill in the registration form and click **Submit**.  
You will be redirected to the download page, and **RPAExpressInstaller.exe** (**18 MB**) will be downloaded automatically.

All the required components will be downloaded during installation.

Due to network issues or firewalls, the download of additional packages may not succeed. Click [here for a workaround](rpax-installation-workaround).

## Installing RPA Express

> **Note**: Before RPA Express installation, make sure your machine meets the expected [System Requirements](system-requirements).

1. Run **RPAExpressInstaller.exe** to install RPA Express.
2. To start installation process, you must accept the License Agreement. Click the **License Agreement** to view it.
3. Read the License Agreement carefully. Use the scroll bar or press the page down key, if necessary.
4. To accept, check **I accept the terms in the License Agreement** and press **Next**.
> **Note**: The **I agree to share usage analytics, that can help WorkFusion to improve its products** checkbox is optional to check.
5. Enter the email you used when [registering]() for RPA Express.

## Selecting Installation Type

Depending on the Server-Workstation configuration 3 types of RPA Express installation are available:

1. **Server + Workstation** — Combined installation of Application Server and Developer machine within the single environment.
1. **RPA Express Installation. Server** — Installation of Application Server, including **[Control Tower]()**, **Bots**,**[WorkSpace]()**, **S3-like File Storage**, **[OCR]()**, and **[Platform Monitor]()** on a single environment shared among several Developer machines enabling collaborative work to publish Bots and run them in Control Tower.
1. **RPA Express Installation. Workstation** — Installation of a Developer machine, including **Bots**, **OCR**, **[Platform Monitor]()**, and **[WorkFusion Studio]()**.

### Variant 1. Server + Workstation

1. On the **Installation Type** screen, select both **RPA Express Application Server** and **RPA Express Workstation** and click **Next**.  
![](assets/rpax/user-guide/52404569.png)
2. Enter the number of bots to be run on Server.

> **Note**: To use **multiple bots**, your machine need the following configuration:
>
> - Windows Server: 2012, 2012 R2, or 2016
> - 2 GB of RAM for each additional bot. Refer to [System Requirements](system-requirements).
>
> If the machine has non-server OS, you cannot install _more than 1 bot_ on it.

3. Enter the default username and password for RPA Express Administrator.

> **Note**:
>
> Pick **any username and password** at your choice, though observe the following requirements:
>
> - The username should be 5 to 20 characters long and contain lowercase Latin letters and digits. Special symbols are not allowed.
> - The password should be 8 to 20 characters long and contain Latin letters and digits. Special symbols are not allowed.

The username and the password entered at this step are used to log in to **Control Tower**, **WorkSpace**, **Platform Monitor**, and **File Storage**.

### Variant 2. Server

Server installation is only available on Windows Server 2012, 2012 R2,
and 2016.

1. Select **RPA Express Application Server** and click **Next**.
2. Enter the number of bots to be run on Server.  

The number of bots depends on the server resources and the tasks the bots perform (refer to [System Requirements](system-requirements)). It is recommended to reserve about 2 GB of RAM for each bot.

3. Enter the default username and password for RPA Express Administrator.

> **Note**:
>
> Pick **any username and password** at your choice, though observe the following requirements:
>
> - The username should be 5 to 20 characters long and contain lowercase Latin letters and digits. Special symbols are not allowed.
> - The password should be 8 to 20 characters long and contain Latin letters and digits. Special symbols are not allowed.

The username and password entered at this step are used to log in to **Control Tower**, **WorkSpace**, **Platform Monitor**, and **File Storage**.

### Variant 3. Workstation

1. Select **RPA Express Workstation** and click **Next**.  
![](assets/rpax/user-guide/52404629.png)
2. Enter the **hostname** or **IP address** of **RPA Express Server**. Provide credentials for **Control Tower**.

**The username and password** entered at this step should match the credentials created on the Server to which the machine connects.

3. Click **Test Connection** to verify that the hostname/IP address and Control Tower credentials are correct.
4. Click **Next** to proceed.

### Licensing Models

1. **Trial** – During the trial period of 30 days, you can use all Pro features available with RPA Express Pro.
1. **RPA Express Starter** – Server + Workstation installation, single local Bot, single local Control Tower user.
1. **RPA Express Pro** – Server and multiple Workstations, multiple Control Tower users, and multiple concurrent Bots depending on hardware resources of the Server machine.

## Finalizing Installation

1. Set the destination folder for RPA Express installation and click **Install**.  
![](assets/rpax/user-guide/52404632.png)
2. Accept security prompts by clicking **Yes**.  
![](assets/rpax/user-guide/42570572.png)  

> **Note**: The window above appears in case the current Windows user does not have Administrator's privileges. User Access Control (UAC) is set to notify the user when apps try to make changes to computer.  

The installation process will start. Wait until the installation is finished.

3. Click **Finish**. As soon as the installation is finished, you will see a welcome message.  
![](assets/rpax/user-guide/42569819.png)

To have a quick walk-through, watch a start tour.  
![](assets/rpax/user-guide/42569817.png)

During the RPA Express installation, [RDS licensing]() is activated automatically. Depending on the number of bots, installation type, and the RDS license current state, installer restart might be required at this stage.

Once RPA Express is installed, you can modify some core features with no need to reinstall. See sections [Modifying Credentials]() and [Changing Number of
Bots]() for more details.

## Updating RPA Express

If you have RPA Express 2.0 already installed, the installer prompts you to update your current version of RPA Express.

Refer to the [How to Update RPA Express]() section for more information on how
to update your RPA Express version.

## Viewing RPA Express Tray Menu

The **RPA Express** icon will appear in the system tray. To open the RPA
Express panel, click the tray icon.

![](assets/rpax/user-guide/42569820.png)  

From the RPA Express tray menu, you can view statuses of all RPA Express components and manage them. See [Platform Monitor](Platform_Monitor) for more details.

To access **WorkFusion Studio**, select it in the tray menu. See [How to Use RPA Recorder]() for more details.

![](assets/rpax/user-guide/42569821.png)

Keep in mind that it can take several minutes to start **Control Tower** and **WorkSpace**.

------------------------------------------------------------------------

## Need help? 

*Having a problem with installation?* Ask a question on our [Forum](https://forum.workfusion.com/).
