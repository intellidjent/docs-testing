---
title: Welcome
---

Welcome to the user documentation for RPA Express. This hands-on guide will help you learn the basics of automation with RPA Express.

If you have questions related to the product and it's capabilities or simply want to get to know more, read [FAQ][1] first. Can't find answers in FAQ or looking for knowledgeable advice, join our [forum][2] and get answers from experts around the globe.

> **System Requirements**  
> You would need a Windows machine to use RPA Express. Please make sure it meets the [System Requirements]() before you proceed.

[1]: https://kb.workfusion.com/display/RPAe/FAQ
[2]: https://forum.workfusion.com/
