---
title: System Requirements
---

## General System Requirements

The below hardware enables you to install RPA Express and run 1 bot
concurrently.

### Server + Workstation

- **OS**: Windows (1 bot): 7, 8, 8.1, 10; Windows Server: 2012, 2012 R2, 2016
- **OS Type**: 64 bit
- **CPU**: 4 cores at 2.8 GHz
- **RAM**: Minimum 8 GB of free memory (16 GB recommended)
- **Disk Space**: 8 GB

### Server

- **OS**: Windows Server: 2012, 2012 R2, 2016
- **OS Type**: 64 bit
- **CPU**: 4 cores at 2.8 GHz
- **RAM**: Minimum 8 GB of free memory (16 GB recommended)
- **Disk Space**: 10 GB

### Workstation

- **OS**: Windows (1 bot): 7, 8, 8.1, 10; Windows Server: 2012, 2012 R2, 2016
- **OS Type**: 64 bit
- **CPU**: 2 cores at 2.8 GHz
- **RAM**: Minimum 4 GB of free memory (8 GB recommended)
- **Disk Space**: 2 GB

> **General Tip**:
> - RPA Express would need **Administrator permissions** to install all required components.
> - A Solid State Drive (SSD) will make things faster.

## RPA Express Pro System Requirements

To run **multiple concurrent bots**, you need the following configuration:

- **OS**: Windows Server: 2012, 2012 R2, or 2016
- **CPU, RAM**: 1 CPU and 2 GB RAM per each bot.
    - *For example, if you want to run 4 bots under the Server + Workstation installation type, you need at least 7 CPU at 2.8 GHz and 14 GB RAM.*
    - *The needed CPU is a combination of your total physical and virtual cores.*

You may need RDS CAL per each bot. Refer to RDS Licensing and Multiple Bots per Server for more details.

> **Note**: To ensure smooth performance, we recommend running no more than 16 bots per Windows Server concurrently.

Refer to [Deployment Diagrams ](Deployment_Diagrams) for
more details about available options.

### Requirements Calculation

- **OS**: Windows Server: 2012, 2012 R2, 2016
- **OS Type**: 64 bit
- **Disk Space**: 8 GB
- **Type**: Server

| Bots  | Recommended CPU     | Recommended RAM           |
| :---- | :------------------ | :------------------------ |
| 1–2   | 4 cores at 2.8 GHz  | 8 GB + 2 GB per each bot  |
| 3–10  | 8 cores at 2.8 GHz  | 12 GB + 2 GB per each bot |
| 11–16 | 16 cores at 2.8 GHz | 28 GB + 2 GB per each bot |

The number of cores in your CPU depends on the actual number of applications used by each bot. Thus, in case of low load, 16 bots can run on 4 cores (CPU).
