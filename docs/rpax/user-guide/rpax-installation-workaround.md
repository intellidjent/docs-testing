---
title: RPA Express Installation Workaround
---

In this case, you will get the following dialog window.

![](assets/rpax/user-guide/48630798.png)

To fix the problem, do as follows.

1.  Click the download mirror link to get redirected to [this
    page](https://www.workfusion.com/rpa-express/download/?download=false).  
    ![](assets/rpax/user-guide/48630799.png)
2.  Click the **Trouble downloading? Click here** link to download the
    full package.

    Full package size is about 2 GB.

    The mirror link is a Dropbox-based file share, which may be blocked
    in your corporate network. In this case, please
    email <rpaexpress@workfusion.com> to figure out the way to get the
    file

3.  Once the package is downloaded, right-click the compressed folder
    and select **Extract all** from the context menu.  
    ![](assets/rpax/user-guide/48630800.png)
4.  By default, the compressed files will extract in the same location
    as the zipped folder. Click the **Browse** button to specify another
    location.  
    ![](assets/rpax/user-guide/48630801.png)
5.  To proceed, press **Extract**.  
    ![](assets/rpax/user-guide/52406827.png)
6.  To launch **RPAExpressInstaller.exe**, right-click the file and
    select **Run as administrator**.  
    ![](assets/rpax/user-guide/48630803.png)
