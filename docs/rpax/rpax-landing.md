---
title: Get Started
---

Here's a series of on-boarding videos that will help you quickly get started with RPA Express.

## Step 1. What is RPA Express?

<iframe width="560" height="315" src="https://www.youtube.com/embed/XxexroVMLUM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Step 2. How to install RPA Express?

<iframe width="560" height="315" src="https://player.vimeo.com/video/272615350" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Step 3. How do I get started?

<iframe width="560" height="315" src="https://www.youtube.com/embed/rVd66AFAeHs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Still got questions?

>  **Help articles**:
>
> - [System Requirements](rpax/user-guide/system-requirements)
> - RPA Express Installation

> **Having issues with installation?**
>
> - Check issues by your-self.
> - Ask on our Forum.
