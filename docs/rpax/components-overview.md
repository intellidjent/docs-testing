---
title: Components Overview
sidebar_label: Components Overview
---

WorkFusion provides full range of components allowing comprehensive
Enterprise automation[^1]. To understand goals of each and how components
work together first let's group them on high level.

![](assets/rpax/components-overview/components-overview.png)

Now let's look at each component description and functions it provides.

## Applications

### WorkSpace

WorkSpace is a web application for manual task fulfillment by Subject Matter Experts.

Related components: Control Tower, Object Storage

Covered functionality:

- Manual tasks queue management.
- Manual task completion by subject matter experts.
- Managing users, roles, teams, qualifications.

### Control Tower

Control Tower is the central managing web application, which orchestrates automation processes and tasks, and provides a user interface for power users, admins and developers.

Related components: WorkSpace, Workflow, RPA, AutoML, OCR, Data Storage

Covered functionality:

- Automation Processes Management
- Creating and editing workflows using GUI tools
- Running and monitoring automations
- Input/output data handling
- User and Role management
- Manual Tasks management
- Workforce management
- System-wide configuration

## Services

### AutoML

> **Note**: Available only via SPA

AutoML finds the best ML model for a particular target function (expressed in a Manual Task), trains it, validates it, and finally predicts the target’s variables.

Related components: Control Tower, Data Storage

WorkFusion SPA includes a native capability called AutoML that automates the data cleansing, model selection and model training work typically done manually by data scientists. The software includes an interface that business people use to tag unstructured data so that ML models can identify patterns in work. Once it has seen enough examples of work patterns, the software predicts with human or better level of confidence how work should be performed. AutoML significantly reduces the amount of time and cost it takes to deploy and scale AI by making it a scalable self-service capability for business people.

![](assets/rpax/components-overview/automl.png)

AutoML can learn not only from Word documents, PDFs and TIFFs but also spreadsheets, emails, text messages, webpages, and comments fields. SPA is unique in the AI arena in that, rather than requiring millions of samples like popular cloud-based AI providers, it can optimally train on 300 to 500 samples before delivering an impactful rate of automation. Each additional sample incrementally increases the automation rate.

Many automation use cases across a business have the same tasks. For example, in banking, a mortgage document and a SWIFT message are different forms of data and are used in different processes, but they both include people’s names and addresses. Transfer Learning, a native feature of WorkFusion SPA, applies learning from one use case to another use case with the same task. This reduces the amount of time it takes ML to learn and deploy.

### RPA

RPA Cluster is a scalable computing environment that executes user scenarios within desktop or web applications. It takes control of the keyboard and mouse pointer to emulate user inputs.

RPA's main goal is automation of user scenarios within desktop and web applications.

Related components:	Control Tower                                       |

### OCR

OCR engine processes the scanned documents and converts unstructured, image-based data into structured character documents in several formats (html, xml).

Related components: Control Tower, WorkSpace, Data Storage

### Workflow

Workflow Engine executes automation processes authored in Control Tower. This is the server application configured within Control Tower.

Automation Process execution according to process definition created in Control Tower. Execution is handled in different computing environments—RPA, WorkSpace, OCR and AutoML.

Related components: Control Tower, RPA, WorkSpace, AutoML

### Analytics

> **Note**: Available only in SPA

The Analytics web application provides comprehensive dashboards and reporting capabilities.

Related components: Control Tower, WorkSpace

WorkFusion Analytics Dashboards are a reporting mechanism that aggregate and display metrics and key performance indicators (KPIs). Dashboards help improve decision making by revealing and communicating in-context insight into business performance.

![](assets/rpax/components-overview/analytics.png)

A dashboard consists of one or more charts, tables, and numeric metrics. Each SPA setup provides package of out of the box dashboards. In addition to out of the box set, WorkFusion SPA team or WorkFusion partners can also provide custom on demand dashboards to visualize your automation unique outcomes the best way possible.

### Platform Monitor

Platform Monitor is responsible for the centralized monitoring of SPA platform components.

Related components: Control Tower, WorkSpace, RPA, OCR, Analytics

Covered functionality:

- Monitoring overall platform component health.
- Monitoring hardware utilization and health.
- Alerting on issues.
- Providing API to integrate with third-party application monitoring tools.

### Secrets Vault

Secrets Vault is a secured storage destination for sensitive data.

Related components: Control Tower, RPA

This is mostly related to the solution’s non-functional requirements:

- Storing authentication data (login/password) for applications involved in RPA and providing secure access to it during business process execution.
- Storing authentication data for WorkFusion SPA inter-component interaction.

### Database

Database stores business process definitions, runtime and tracking information on their executions—including RPA and Cognitive automations. It's a reliable storage of the designed automation implementation.

Related components: Control Tower, WorkSpace, Analytics

### Object Storage

Object Storage keeps binary file type content, such as OCR results, AutoML models, screenshots and reports generation by automation. It's a reliable storage of OCR results and trained AutoML models.

Related components: Workflow, OCR, AutoML

[^1]: This is the note
